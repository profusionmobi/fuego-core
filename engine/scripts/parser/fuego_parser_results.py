#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 Toshiba corp.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
results.py - This library is used to merge run.json files into a single
             results.json file
By Daniel Sangorrin (August 2017)
"""

import os, json, collections
from filelock import FileLock
from fuego_parser_utils import split_test_id, get_test_case

results_debug_bitmask=4
debug = 0

try:
    if int(os.environ['FUEGO_DEBUG']) & results_debug_bitmask:
        debug=1
        dprint("Fuego results debug messages active")
except:
    pass

def dprint(msg):
    if debug:
        print "DEBUG:", msg

def extract_test_case_ids(ref):
    test_case_ids = []
    for test_set in ref['test_sets']:
        test_set_id = test_set['name']
        for test_case in test_set['test_cases']:
            test_case_id = test_set_id + '.' + test_case['name']
            test_case_ids.append(test_case_id)
    return test_case_ids

def update_results_json(test_logdir, TESTDIR, REF_JSON):
    dprint("In update_results_json")
    results_data = { "test_name" : TESTDIR }

    try:
        with open(REF_JSON) as ref_file:
            ref = json.load(ref_file, object_pairs_hook=collections.OrderedDict)
        ref_test_case_ids = extract_test_case_ids(ref)
    except:
        print "reference.json not available"
        ref_test_case_ids = None

    # look for run.json files on each build folder
    for folder in sorted(os.listdir(test_logdir)):
        path = test_logdir + '/' + folder
        if not os.path.isdir(path):
            continue
        run_file = path + '/run.json'
        try:
            with open(run_file, 'r') as f:
                run_data = json.load(f)
        except:
            print('WARNING: Unable to open ' + run_file)
            continue

        try:
            if not ref_test_case_ids:
                test_case_ids = extract_test_case_ids(run_data)
                dprint("Test cases extracted from run data: " + str(test_case_ids))
            else:
                test_case_ids = ref_test_case_ids

            for test_case_id in test_case_ids:
                dprint("Processing test_case_ide: " + str(test_case_id))
                test_set_name, test_case_name = split_test_id(test_case_id)
                test_case = get_test_case(test_case_id, run_data)

                key = '%s-%s-%s-%s-%s' % (run_data['metadata']['board'],
                                        run_data['metadata']['test_spec'],
                                        run_data['metadata']['kernel_version'],
                                        test_set_name,
                                        test_case_name)

                dprint("Key: " + key)
                if key not in results_data:
                    results_data[key] = {
                        "test_plan": run_data['metadata']['test_plan'],
                        "test_spec": run_data['metadata']['test_spec'],
                        'board' : run_data['metadata']['board'],
                        'kernel_version' : run_data['metadata']['kernel_version'],
                        'toolchain' : run_data['metadata']['toolchain'],
                        'test_set' : test_set_name,
                        'test_case' : test_case_name
                    }
                    results_data[key]['status'] = []
                    results_data[key]['measurements'] = {}
                    results_data[key]['build_number'] =[]
                    results_data[key]['start_time'] = []
                    results_data[key]['duration_ms'] = []

                results_data[key]['status'].append(test_case['status'])
                results_data[key]['build_number'].append(run_data['metadata']['build_number'])
                results_data[key]['start_time'].append(run_data['metadata']['start_time'])
                results_data[key]['duration_ms'].append(run_data['duration_ms'])

                measurements = test_case.get('measurements', [])
                for measure in measurements:
                    value = measure.get('measure', 0)
                    status = measure.get('status', "SKIP")
                    if measure['name'] not in results_data[key]['measurements']:
                        results_data[key]['measurements'][measure['name']] = {'measure': [value], 'status': [status]}
                    else:
                        results_data[key]['measurements'][measure['name']]['measure'].append(value)
                        results_data[key]['measurements'][measure['name']]['status'].append(status)
        except:
            print('WARNING: Unable to parse ' + run_file)
            continue

    RESULTS_JSON = test_logdir + '/results.json'
    print "Writing merged results to ", RESULTS_JSON
    with FileLock(RESULTS_JSON + '.lock'):
        with open(RESULTS_JSON, 'w') as f:
            f.write(json.dumps(results_data, sort_keys=True, indent=4, separators=(',', ': ')))
