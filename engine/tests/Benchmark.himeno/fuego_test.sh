tarball=himeno.tar.bz2

function test_build {
    CFLAGS+=" -O3"  
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS"
}

function test_deploy {
	put bmt  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR && ./bmt"  
}


