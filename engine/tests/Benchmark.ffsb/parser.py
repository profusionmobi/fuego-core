#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys
sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

regex_string_main = '^\s*(readall|create|append|delete|stat|writeall|writeall_fsync|open_close|create_fsync|append_fsync)\s{1}:\s*(\d+\.?\d*)\s*(\d+\.?\d*)\s*(\d+\.?\d*)%\s*(\d+\.?\d*)%\s*(NA|\d+\.?\d*).*$'
regex_string_throughput = '^(Read|Write)\s{1}Throughput:\s*(\d+\.?\d*)(\D{1})B/sec$'
regex_string_syscalls_latency = '^\[\s*(\D+)\]\s*(\d+\.?\d*)\s*(\d+\.?\d*)\s*(\d+\.?\d*).*$'

measurements = {}

matches = plib.parse_log(regex_string_main)
if matches:
    for test in matches:
        measurements['Main.'+test[0]] = [{"name": "Transactions", "measure" : float(test[1])}]
        measurements['Main.'+test[0]].append({"name": "TPS", "measure" : float(test[2])})
        measurements['Main.'+test[0]].append({"name": "TPercent", "measure" : float(test[3])})
        measurements['Main.'+test[0]].append({"name": "OpWeight", "measure" : float(test[4])})

matches = plib.parse_log(regex_string_throughput)
if matches:
    if matches[0][2] == 'K':
        r_mul = 1;
    elif matches[0][2] == 'M':
        r_mul = 1000;
    if matches[1][2] == 'K':
        w_mul = 1;
    elif matches[1][2] == 'M':
        w_mul = 1000;
    measurements['Throughput.Read'] = [{"name": "Throughput", "measure" : '%.2f' % (float(matches[0][1]) * r_mul)}]
    measurements['Throughput.Write'] = [{"name": "Throughput", "measure" : '%.2f' % (float(matches[1][1]) * w_mul)}]

matches = plib.parse_log(regex_string_syscalls_latency)
if matches:
    for test in matches:
        measurements['Syscall_latency.'+test[0]] = [{"name": "Min", "measure" : float(test[1])}]
        measurements['Syscall_latency.'+test[0]].append({"name": "Avg", "measure" : float(test[2])})
        measurements['Syscall_latency.'+test[0]].append({"name": "Max", "measure" : float(test[3])})

sys.exit(plib.process(measurements))
