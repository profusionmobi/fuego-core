# gitrepo=https://github.com/esnet/iperf.git
tarball=iperf-3.1.3-source.tar.gz

function test_build {
    LDFLAGS=-static ./configure --host=$HOST --prefix=$(pwd)/build
    make
}

function test_deploy {
    put src/iperf3  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    # FIXTHIS: validate the server and client ips, and make sure they can communicate
    # FIXTHIS: add functionality to setup the server using a board file
    # Get the server ip
    IPERF3_SERVER_IP=${BENCHMARK_IPERF3_SERVER_IP:-$SRV_IP}
    if [ -z "$IPERF3_SERVER_IP" ]; then
        echo "ERROR: set the server ip on the spec or board file"
        return 1
    fi

    echo "Starting iperf3 client on the target (CLIENT IP: $IPADDR, SERVER IP: $IPERF3_SERVER_IP)"
    json_file=$(mktemp)
    for i in 1 2 3 4 5 6; do
        cmd "$BOARD_TESTDIR/fuego.$TESTDIR/iperf3 -V -c $IPERF3_SERVER_IP -J --logfile $BOARD_TESTDIR/fuego.$TESTDIR/output.json --get-server-output $BENCHMARK_IPERF3_CLIENT_PARAMS" && break || \
            echo "The server seems busy running another iperf3 test. Trying again in 30 seconds"

        # that was our last try so abort the job
        if [ $i -eq 6 ]; then
            abort_job "The server seems busy running another iperf3 test."
        fi

        # remove the json file before retrying, otherwise it gets appended
        cmd "rm -f $BOARD_TESTDIR/fuego.$TESTDIR/output.json"
        sleep 60
    done
    rm -f $json_file

    report "cat $BOARD_TESTDIR/fuego.$TESTDIR/output.json"
}

function test_cleanup {
    kill_procs iperf3
}
