tarball=../rt-tests/rt-tests-v1.1.1.tar.gz

NEED_ROOT=1

function test_pre_check {
    assert_define BENCHMARK_SVSEMATEST_PARAMS
}

function test_build {
    patch -p1 -N -s < $TEST_HOME/../rt-tests/0001-Add-scheduling-policies-for-old-kernels.patch
    make NUMA=0 svsematest
}

function test_deploy {
    put svsematest  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    # svsematest does not support a option for printing a summary only on exit.
    # So, We get some lines at the end of the command's output.
    # The number for getting the lines depends on the cpu number of target machine.
    target_cpu_number=$(cmd "nproc")
    getting_line_number=$(( $target_cpu_number + $target_cpu_number ))
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./svsematest $BENCHMARK_SVSEMATEST_PARAMS | tail -$getting_line_number"
}
