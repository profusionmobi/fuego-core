tarball=lmbench3.tar.gz

function test_build {
   mkdir -p SCCS
   touch SCCS/s.ChangeSet
   cd scripts
   patch -p0 < $TEST_HOME/lmbench3.config-run.patch
   patch -p0 < $TEST_HOME/lmbench.patch
   patch -p0 < $TEST_HOME/lmbench3.mem64.patch
   cd ../src
   patch -p0 < $TEST_HOME/bench.h.patch
   cd ..
   CFLAGS+=" -g -O"
   export OS=$PREFIX
   LMBENCH_OS=$(scripts/os)
   make OS="$LMBENCH_OS" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS"
}

function test_deploy {
   put *  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
   # some trickery to get the directory right
   if [ -n "$PREFIX" ] ; then
      LMBENCH_OS=`ls ./bin`
   else
      LMBENCH_OS=$(scripts/os)
   fi
   safe_cmd "rm -rf $BOARD_TESTDIR/fuego.$TESTDIR/results"
   safe_cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR/scripts; OS=$LMBENCH_OS ./config-run"
   safe_cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR/scripts; OS=$LMBENCH_OS ./results"
   report "cd $BOARD_TESTDIR/fuego.$TESTDIR/scripts; ./getsummary ../results/$LMBENCH_OS/*.0"
}

function test_cleanup {
   kill_procs lmbench lat_mem_rd par_mem
}


